$(document).ready(function() {
	/*Fancy Box*/
	$(".fancybox").fancybox({
		openEffect	: 'fade',
		closeEffect	: 'fade'
	});

	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'fade',
		closeEffect	: 'fade'
	});
	
	var current_char = 'char1'
	
	$('.subpage').hide();
	$('#char1').show();
	
	var page_width = 1024;
	
	/*Main Navigation*/
	$('#navigation a').click(function(e){
		e.preventDefault();
		var clicked = $(this).attr('rel');
		
		if(clicked) {
			var move = clicked * page_width;
			$('#navigation li').removeClass('active');
			$(this).parent().addClass('active');
			$('#pages').animate({ right: move },1000);
		}
	});
	
	/*Cast Page Navigation*/
	$('#cast_nav a').click(function(e){
		e.preventDefault();
		var clicked = $(this).attr('rel');
		
		if(clicked !== current_char){
			$('#'+current_char).fadeOut('slow', function(){
				$('#'+clicked).fadeIn('slow');
				current_char = clicked;
			});
		}
	});
});